﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;

namespace Multiscale
{
    public partial class Form1 : Form
    {
        Cell[,] array;
        Cell[,] tmpArray;
        Color[] colorArray;
        Random rand = new Random();
        int x, y, nucleons;
        int mouseX;
        int mouseY;
        int cellSize = 1;
        int counter;
        int inclusions;
        int clickedNucleonIndex = -1;
        Color clickedNucleonColor;
        Color defaultColor = new Color();
        Bitmap createBmp;
        Graphics createGraphic;
        String inclusionType;
        String structureType;
        int inclusionSize;
        bool growNucleonsFinished = false;
        bool generateInclusionFinished = false;
        bool generateNucleonsFinished = false;
        bool generateBoundariesFinished = false;
        bool nucleonsSelected = false;
        bool isOnEnge = false;
        String typeOfSimulation;
        List<int> neighborhood = new List<int>();
        Dictionary<int, Color> myHashMap1 = new Dictionary<int, Color>();
        Dictionary<Color, int> colorHashMap = new Dictionary<Color, int>();


        public Cell[,] setInitialArray(Cell[,] a)
        {
            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    a[i, j] = new Cell();
                }
            }
            return a;
        }

        public Form1()
        {
            InitializeComponent();
            sizeX.Text = "300";
            sizeY.Text = "300";
            nucleonAmount.Text = "50";
            inclusion.Text = "5";
            typeOfInclusion.Text = "square";
            sizeOfInclusion.Text = "3";
            simulationType.Text = "Von Neumann";
            scrollBarProbability.Value = 20;
            structureBox.Text = "Substructure";
            defaultColor = Color.FromArgb(255, 105, 180);
        }

        public void clearIgnoredValues()
        {
            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                        array[i, j].setIgnore(false);
                        tmpArray[i, j].setIgnore(false);   
                }
            }
        }

        public void setIgnoreValues()
        {
            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    if (myHashMap1.ContainsKey(array[i, j].getIndex()))
                    {
                        array[i, j].setIgnore(true);
                        tmpArray[i, j].setIgnore(true);
                    }
                }
            }
        }

        public void clearNotIgnoredCells()
        {
            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    if (!array[i, j].getIgnore())
                    {
                        array[i, j].setIndex(0);
                        array[i, j].setState(false);
                        tmpArray[i, j].setIndex(0);
                        tmpArray[i, j].setState(false);
                    }
                }
            }
        }

        public void fillArrayWithRandomNucleons()
        {
            structureType = structureBox.Text;

            if (nucleonsSelected)
            {
                clearIgnoredValues();
                setIgnoreValues();
                clearNotIgnoredCells();
            }
            int i = 0;
            while (i < nucleons)
            {
                int randX = rand.Next(x);
                int randY = rand.Next(y);
                if ((array[randX, randY].getState() == false) && (array[randX, randY].getInclusions() == false))
                {
                    array[randX, randY].setState(true);
                    array[randX, randY].setIndex(i);
                    tmpArray[randX, randY].setState(true);
                    tmpArray[randX, randY].setIndex(i);
                    i++;
                }
                else
                    continue;
            }
            drawPictureBox();

        }

        public void generateBoundaries()
        {
            if (growNucleonsFinished)
            {
                for (int i = 0; i < x; i++)
                {
                    for (int j = 0; j < y; j++)
                    {
                        if (j == 0)
                        {

                            int firstCellIndex = array[i, y - 1].getIndex();
                            int secondCellIndex = array[i, 0].getIndex();
                            if (firstCellIndex != secondCellIndex)
                            {
                                array[i, y - 1].setBoundary(true);
                                array[i, 0].setBoundary(true);
                            }
                        }
                        else
                        {
                            int firstCellIndex = array[i, j - 1].getIndex();
                            int secondCellIndex = array[i, j].getIndex();
                            if (firstCellIndex != secondCellIndex)
                            {
                                array[i, j - 1].setBoundary(true);
                                array[i, j].setBoundary(true);
                            }
                        }

                    }
                }

                for (int i = 0; i < y; i++)
                {
                    for (int j = 0; j < x; j++)
                    {
                        if (i == 0)
                        {

                            int firstCellIndex = array[x - 1, j].getIndex();
                            int secondCellIndex = array[0, j].getIndex();
                            if (firstCellIndex != secondCellIndex)
                            {
                                array[x - 1, j].setBoundary(true);
                                array[0, j].setBoundary(true);
                            }
                        }
                        else
                        {
                            int firstCellIndex = array[i - 1, j].getIndex();
                            int secondCellIndex = array[i, j].getIndex();
                            if (firstCellIndex != secondCellIndex)
                            {
                                array[i - 1, j].setBoundary(true);
                                array[i, j].setBoundary(true);
                            }
                        }

                    }
                }
                generateBoundariesFinished = true;
            }
            else
            {
                MessageBox.Show("First, generate and grow nucleons please.");
            }
        }

        public void fillArrayWithRandomInclusions()
        {
            inclusionType = typeOfInclusion.Text; 
            inclusionSize = Int32.Parse(sizeOfInclusion.Text);  
            if (generateInclusionFinished == false)  // sprawdzam czy wygenerowalam juz inkluzje, jesli tak to wyskakuje komunikat i musze utworzyc bitmape od nowa
            {
                if (inclusionType == "circular") 
                {
                    if (inclusionSize > 0 && inclusionSize <= 50) 
                    {
                        int i = 0;  
                        while (i < inclusions)
                        {
                            int randX = rand.Next(x); 
                            int randY = rand.Next(y);
                            int currentCellIndex; // obecny indeks komorki do sprawdzania czy inkluzja na granicy
                            if (array[randX, randY].getInclusions() == false) 
                            {
                                isOnEnge = false;  // ustawiam flage inkluzja na granicy na false bo jeszcze nie wiem czy jest
                                if ((randX - inclusionSize) >= 0 && (randX + inclusionSize) <= (x - 1) && (randY - inclusionSize) >= 0 && (randY + inclusionSize) <= (y - 1)) 
                                {
                                    currentCellIndex = array[randX, randY].getIndex(); // ustawiam obecny indeks komorki w ktorej chce utworzyc inkluzje na index Nukleonu
                                    //drawing circular inclusion
                                    int count = 0;  // rysuje kolo
                                    int radius = inclusionSize * 2 + 1;
                                    float radiusSq = (radius * radius) / 4;
                                    if (growNucleonsFinished == true) // sprawdzam czy generuje inkluzje juz po wygenerowaniu nukelonow czy przed 
                                    {
                                        // first loop only iterate throw all cell which we want to fill and check if inclusion is on edge
                                        for (int l = (randY - inclusionSize); l <= (randY + inclusionSize); l++) // przechodze przez wszystkie komorki ktorymi mam wypelnic na czarno inkluzje w poziomie
                                        {
                                            float yDiff = l - randY;
                                            float threshold = radiusSq - (yDiff * yDiff);
                                            for (int k = (randX - inclusionSize); k <= (randX + inclusionSize); k++) // przechodze przez wszystkie komorki ktorymi mam wypelnic na czarno inkluzje w pionie
                                            {
                                                count++;
                                                float d = k - randX;
                                                if (((d * d) > threshold) == false)
                                                {
                                                    //check index - if on edge index or no
                                                    if (currentCellIndex != array[k, l].getIndex()) // tutaj sprawdzam obecny indeks komorki ktora mam wypelnic na czarno jako inkluzje czy jej indeks jest inny niz poprzedniej komorki ktora wypelnialem, jesli tak to znaczy ze jestem na granicy i ustawiam flage isOnEdge na true
                                                    {
                                                        isOnEnge = true;
                                                    }
                                                }
                                            } // na koncu tej petli wiem czy inkluzja do wypelnienia jest na granicy, w tej petli nie wypelniam komorek tylko je sprawdzam
                                        }
                                        // check if inclusion is on edge
                                        if (isOnEnge) // jesli sprawdzana wczesniej inkluzja jest na granicy to wchodze w ten warunek i ..
                                        { //draw circular
                                            for (int l = (randY - inclusionSize); l <= (randY + inclusionSize); l++)
                                            {
                                                float yDiff = l - randY;

                                                float threshold = radiusSq - (yDiff * yDiff);
                                                for (int k = (randX - inclusionSize); k <= (randX + inclusionSize); k++)
                                                {
                                                    count++;
                                                    float d = k - randX;
                                                    if (((d * d) > threshold) == false)
                                                    {
                                                        array[k, l].setState(true); 
                                                        array[k, l].setInclusions(true);
                                                        tmpArray[k, l].setState(true);  
                                                        tmpArray[k, l].setInclusions(true);
                                                    }
                                                }
                                            }
                                        } // if inclusion is not on edge, then find one more time
                                        else // jesli dana inkluzja nie byla na granicy to zmienjszam licznik (i) aby wylosowac inkluzje jeszcze raz
                                        {
                                            i--;
                                        }
                                    }
                                    else // bifore grow nucleons
                                    {  // here is drawing inclusion without checking if it is on edge
                                        for (int l = (randY - inclusionSize); l <= (randY + inclusionSize); l++) // mamy tutaj po prostu wypelninie danych komorek aby powstalo kolo 
                                        {
                                            float yDiff = l - randY;

                                            float threshold = radiusSq - (yDiff * yDiff);
                                            for (int k = (randX - inclusionSize); k <= (randX + inclusionSize); k++)
                                            {
                                                count++;
                                                float d = k - randX;
                                                if (((d * d) > threshold) == false)
                                                { 
                                                    array[k, l].setState(true);
                                                    array[k, l].setInclusions(true);
                                                    tmpArray[k, l].setState(true);
                                                    tmpArray[k, l].setInclusions(true);
                                                }
                                            }
                                        }
                                    }

                                }
                                else  // jesli inkluzja wylosowana jest w miejscu gdzie wykroczyla by za tablice to daje continue, nie zwiekszam licznika i losuje inkluzje jeszcze raz
                                {
                                    continue;
                                }

                                drawPictureBox();

                                i++;
                            }
                            else  // jesli wylosowane wspolrzedne sa juz na innej inkluzji to tez losuje jeszcze raz
                                continue;
                        }
                        generateInclusionFinished = false; // ustawiam flage ze wygenerowalem juz iinkluzje i nie moge zrobic tego drugi raz 
                    }
                    else
                    {
                        MessageBox.Show("Size of square inclusion should be between 1-50.");
                        return;
                    }
                }
                else if (inclusionType == "square")  
                {
                    if (inclusionSize > 0 && inclusionSize <= 50)
                    {
                        int i = 0;
                        while (i < inclusions)
                        {
                            int randX = rand.Next(x);
                            int randY = rand.Next(y);
                            int currentCellIndex;

                            if (array[randX, randY].getInclusions() == false)
                            {
                                isOnEnge = false;
                                if ((randX - inclusionSize) >= 0 && (randX + inclusionSize) <= (x - 1) && (randY - inclusionSize) >= 0 && (randY + inclusionSize) <= (y - 1))
                                {
                                    currentCellIndex = array[randX, randY].getIndex();
                                    int count = 0;
                                    if (growNucleonsFinished == true)
                                    {
                                        for (int k = (randX - inclusionSize); k <= (randX + inclusionSize); k++)
                                        {
                                            for (int l = (randY - inclusionSize); l <= (randY + inclusionSize); l++)
                                            {
                                                if (array[k, l].getInclusions() == false)
                                                {
                                                    count++;

                                                    if (currentCellIndex != array[k, l].getIndex())
                                                    {
                                                        isOnEnge = true;
                                                    }
                                                }
                                                else
                                                {
                                                    continue;
                                                }
                                            }
                                        }
                                        if (isOnEnge)
                                        {
                                            for (int k = (randX - inclusionSize); k <= (randX + inclusionSize); k++)
                                            {
                                                for (int l = (randY - inclusionSize); l <= (randY + inclusionSize); l++)
                                                {
                                                    if (array[k, l].getInclusions() == false)
                                                    {
                                                        count++;
                                                        array[k, l].setState(true);
                                                        array[k, l].setInclusions(true);
                                                        tmpArray[k, l].setState(true);
                                                        tmpArray[k, l].setInclusions(true);
                                                    }
                                                    else
                                                    {
                                                        continue;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            i--;
                                        }
                                    }
                                    else
                                    {
                                        for (int k = (randX - inclusionSize); k <= (randX + inclusionSize); k++)
                                        {
                                            for (int l = (randY - inclusionSize); l <= (randY + inclusionSize); l++)
                                            {
                                                if (array[k, l].getInclusions() == false)
                                                {
                                                    count++;

                                                    array[k, l].setState(true);
                                                    array[k, l].setInclusions(true);
                                                    tmpArray[k, l].setState(true);
                                                    tmpArray[k, l].setInclusions(true);
                                                }
                                                else
                                                {
                                                    continue;
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    continue;
                                }

                                drawPictureBox();

                                i++;
                            }
                            else
                                continue;
                        }
                        generateInclusionFinished = false;
                    }
                    else
                    {
                        MessageBox.Show("Size of circular inclusion should be between 1-50.");
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("Invalid type of inclusion.");
                    return;
                }
            }
        }

        public void drawPictureBox()
        {
            createBmp = new Bitmap(x * cellSize, y * cellSize);
            createGraphic = Graphics.FromImage(createBmp);
            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    if (array[i, j].getState())
                    {
                        if (array[i, j].getBoudary())
                        {
                            createGraphic.FillRectangle(Brushes.Black, i * cellSize, j * cellSize, cellSize, cellSize);
                        }
                        else if (array[i, j].getIgnore() && (structureType == "Dual Phase"))    //if dual phase ignore color and color this grow nucleon on defaulColour
                        {
                            createGraphic.FillRectangle(new SolidBrush(defaultColor), i * cellSize, j * cellSize, cellSize, cellSize);
                        }
                        else if ((array[i, j].getInclusions() == false))    //generate random colors nucleons
                        {
                            createGraphic.FillRectangle(new SolidBrush(colorArray[array[i, j].getIndex()]), i * cellSize, j * cellSize, cellSize, cellSize);
                        }
                        else if (array[i, j].getInclusions())
                        {
                            createGraphic.FillRectangle(Brushes.Black, i * cellSize, j * cellSize, cellSize, cellSize);
                        }
                    }
                    else createGraphic.FillRectangle(Brushes.White, i * cellSize, j * cellSize, cellSize, cellSize);
                }
            }
            pictureBox1.Image = createBmp;
        }

        public void inspectArray()
        {
            counter = x * y;
            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    if ((array[i, j].getState() == false) && (array[i, j].getInclusions() == false) && (array[i, j].getIgnore() == false))
                    {
                        int index = array[i, j].getIndex();
                        if (typeOfSimulation == "Von Neumann")
                        {
                            growNucleonsVonNeumann(i, j);
                        }
                        else if (typeOfSimulation == "Moore")
                        {
                            growNucleonsMoore(i, j);
                        }
                        else if (typeOfSimulation == "Moore with shape control")
                        {
                            shapeControll(i, j);
                        }
                    }
                    else
                    {
                        counter--; // count how many cells are still empty
                    }
                }
            }
           
        }

        public void rewriteArray()
        {
            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    array[i, j].setState(tmpArray[i, j].getState());
                    array[i, j].setIndex(tmpArray[i, j].getIndex());
                    array[i, j].setInclusions(tmpArray[i, j].getInclusions());
                }
            }
        }

        public void growNucleonsVonNeumann(int placeX, int placeY)
        {

            neighborhood.Clear();

            // check if checked nucleon is out of array
            // ============ check left =============
            if (placeX == 0)
            { // check if nucleon is on first index of array (X)
                if ((array[x - 1, placeY].getState()) && (array[x - 1, placeY].getInclusions() == false) && (array[x - 1, placeY].getIgnore() == false))
                {
                     // add neighbornhood Index to the list
                    neighborhood.Add(array[x - 1, placeY].getIndex());
                }
                else if ((array[x - 1, placeY].getState() == true) && (array[x - 1, placeY].getInclusions() == true))
                {
                    tmpArray[x - 1, placeY].setState(true); 
                    tmpArray[x - 1, placeY].setInclusions(true);
                } else if (array[x - 1, placeY].getState() && array[x - 1, placeY].getIgnore()) {
                    tmpArray[x - 1, placeY].setState(true); 
                    tmpArray[x - 1, placeY].setIgnore(true);
                }
            }
            else
            {
                if ((array[placeX - 1, placeY].getState()) && (array[placeX - 1, placeY].getInclusions() == false) && (array[placeX - 1, placeY].getIgnore() == false))
                {
                    neighborhood.Add(array[placeX - 1, placeY].getIndex());
                }
                else if ((array[placeX - 1, placeY].getState() == true) && (array[x - 1, placeY].getInclusions() == true))
                {
                    tmpArray[placeX - 1, placeY].setState(true); 
                    tmpArray[placeX - 1, placeY].setInclusions(true);
                }
                else if (array[placeX - 1, placeY].getState() && array[placeX - 1, placeY].getIgnore())
                {
                    tmpArray[placeX - 1, placeY].setState(true); 
                    tmpArray[placeX - 1, placeY].setIgnore(true);
                }
            }
            // ============ check top ===============
            if (placeY == 0)
            { // check if nucleon is on first index of array (Y)
                if ((array[placeX, y - 1].getState()) && (array[placeX, y - 1].getInclusions() == false) && (array[placeX, y - 1].getIgnore() == false))
                {
                    neighborhood.Add(array[placeX, y - 1].getIndex());
                }
                else if ((array[placeX, y - 1].getState() == true) && (array[x - 1, placeY].getInclusions() == true))
                {
                    tmpArray[placeX, y - 1].setState(true);
                    tmpArray[placeX, y - 1].setInclusions(true);
                }
                else if (array[placeX, y - 1].getState() && array[placeX, y - 1].getIgnore())
                {
                    tmpArray[placeX, y - 1].setState(true); 
                    tmpArray[placeX, y - 1].setIgnore(true);
                }
            }
            else
            {
                if ((array[placeX, placeY - 1].getState()) && (array[placeX, placeY - 1].getInclusions() == false) && (array[placeX, placeY - 1].getIgnore() == false))
                {
                    neighborhood.Add(array[placeX, placeY - 1].getIndex());
                }
                else if ((array[placeX, placeY - 1].getState() == true) && (array[placeX, placeY - 1].getInclusions() == true))
                {
                    tmpArray[placeX, placeY - 1].setState(true);
                    tmpArray[placeX, placeY - 1].setInclusions(true);
                }
                else if (array[placeX, placeY - 1].getState() && array[placeX, placeY - 1].getIgnore())
                {
                    tmpArray[placeX, placeY - 1].setState(true); 
                    tmpArray[placeX, placeY - 1].setIgnore(true);
                }
            }
            // =========== check right ==============
            if (placeX == (x - 1))
            { // check if nucleon is on last index of array (X)
                if ((array[0, placeY].getState()) && (array[0, placeY].getInclusions() == false) && (array[0, placeY].getIgnore() == false))
                {
                    neighborhood.Add(array[0, placeY].getIndex());

                }
                else if ((array[0, placeY].getState() == true) && (array[0, placeY].getInclusions() == true))
                {
                    tmpArray[0, placeY].setState(true);
                    tmpArray[0, placeY].setInclusions(true);
                } else if (array[0, placeY].getState() && array[0, placeY].getIgnore()) {
                    tmpArray[0, placeY].setState(true); 
                    tmpArray[0, placeY].setIgnore(true);
                }
            }
            else
            {
                if ((array[placeX + 1, placeY].getState()) && (array[placeX + 1, placeY].getInclusions() == false) && (array[placeX + 1, placeY].getIgnore() == false))
                {
                    neighborhood.Add(array[placeX + 1, placeY].getIndex());
                }
                else if ((array[placeX + 1, placeY].getState() == true) && (array[placeX + 1, placeY].getInclusions() == true))
                {
                    tmpArray[placeX + 1, placeY].setState(true);
                    tmpArray[placeX + 1, placeY].setInclusions(true);
                }
                else if (array[placeX + 1, placeY].getState() && array[placeX + 1, placeY].getIgnore())
                {
                    tmpArray[placeX + 1, placeY].setState(true);
                    tmpArray[placeX + 1, placeY].setIgnore(true);
                }
            }
            // =========== check bottom =============
            if (placeY == (y - 1))
            { // check if nucleon is on last index of array (Y)
                if ((array[placeX, 0].getState()) && (array[placeX, 0].getInclusions() == false) && (array[placeX, 0].getIgnore() == false))
                {
                    neighborhood.Add(array[placeX, 0].getIndex());
                }
                else if ((array[placeX, 0].getState() == true) && (array[placeX, 0].getInclusions() == true))
                {
                    tmpArray[placeX, 0].setState(true);
                    tmpArray[placeX, 0].setInclusions(true);
                } if (array[placeX, 0].getState() && array[placeX, 0].getIgnore())
                {
                    tmpArray[placeX, 0].setState(true);
                    tmpArray[placeX, 0].setIgnore(true);
                }
            }
            else
            {
                if ((array[placeX, placeY + 1].getState()) && (array[placeX, placeY + 1].getInclusions() == false) && (array[placeX, placeY + 1].getIgnore() == false))
                {
                    neighborhood.Add(array[placeX, placeY + 1].getIndex());
                }
                else if ((array[placeX, placeY + 1].getState() == true) && (array[placeX, placeY + 1].getInclusions() == true))
                {
                    tmpArray[placeX, placeY + 1].setState(true);
                    tmpArray[placeX, placeY + 1].setInclusions(true);
                } if (array[placeX, placeY + 1].getState() && array[placeX, placeY + 1].getIgnore())
                {
                    tmpArray[placeX, placeY + 1].setState(true);
                    tmpArray[placeX, placeY + 1].setIgnore(true);
                }
            }

            if (neighborhood.Any())
            {
                tmpArray[placeX, placeY].setState(true);
                tmpArray[placeX, placeY].setIndex(neighborhood.GroupBy(v => v).First(g => g.Count() == neighborhood.GroupBy(v => v).Max(v => v.Count())).Key);
            }

        }
        public void growNucleonsMoore(int placeX, int placeY)
        {

            List<int> neighborhood = new List<int>();
            List<int> neighborhood2 = new List<int>();

            neighborhood2.Clear();
            neighborhood.Clear();

            // check if grow nucleon is out of array
            // ============ grow left =========
            if (placeX == 0)
            {   // if nucleon is in top left corner
                if (placeY == 0)
                {
                    // swap left top grow cell to right bottom cell of array
                    if (array[x - 1, y - 1].getState() && (array[x - 1, y - 1].getInclusions() == false))
                    {
                        neighborhood.Add(array[x - 1, y - 1].getIndex());
                    }
                    else if (array[x - 1, y - 1].getState() && array[x - 1, y - 1].getInclusions())
                    {
                        tmpArray[x - 1, y - 1].setState(true);
                        tmpArray[x - 1, y - 1].setInclusions(true);
                    }
                    // swap left middle grow cell to right last cell of array
                    if (array[x - 1, placeY].getState() && (array[x - 1, placeY].getInclusions() == false))
                    {
                        neighborhood.Add(array[x - 1, placeY].getIndex());
                    }
                    else if (array[x - 1, placeY].getState() && array[x - 1, placeY].getInclusions())
                    {
                        tmpArray[x - 1, placeY].setState(true);
                        tmpArray[x - 1, placeY].setInclusions(true);
                    }
                    // swap left bottom grow cell to right top cell of array
                    if ((array[x - 1, placeY + 1].getState()) && (array[x - 1, placeY + 1].getInclusions() == false))
                    {
                        neighborhood.Add(array[x - 1, placeY + 1].getIndex());

                    }
                    else if (array[x - 1, placeY + 1].getState() && array[x - 1, placeY + 1].getInclusions())
                    {
                        tmpArray[x - 1, placeY + 1].setState(true);
                        tmpArray[x - 1, placeY + 1].setInclusions(true);
                    }
                    // swap top middle grow cell to bottom cell of array
                    if (array[placeX, y - 1].getState() && (array[placeX, y - 1].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX, y - 1].getIndex());
                    }
                    else if (array[placeX, y - 1].getState() && array[placeX, y - 1].getInclusions())
                    {
                        tmpArray[placeX, y - 1].setState(true);
                        tmpArray[placeX, y - 1].setInclusions(true);
                    }
                    // swap top right grow cell to bottom cell of array
                    if ((array[placeX + 1, y - 1].getState()) && (array[placeX + 1, y - 1].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX + 1, y - 1].getIndex());
                    }
                    else if (array[placeX + 1, y - 1].getState() && array[placeX + 1, y - 1].getInclusions())
                    {
                        tmpArray[placeX + 1, y - 1].setState(true);
                        tmpArray[placeX + 1, y - 1].setInclusions(true);
                    }
                    // grow right middle cell
                    if (array[placeX + 1, placeY].getState() && (array[placeX + 1, placeY].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX + 1, placeY].getIndex());
                    }
                    else if (array[placeX + 1, placeY].getState() && array[placeX + 1, placeY].getInclusions())
                    {
                        tmpArray[placeX + 1, placeY].setState(true);
                        tmpArray[placeX + 1, placeY].setInclusions(true);
                    }
                    // grow right bottom cell
                    if (array[placeX + 1, placeY + 1].getState() && (array[placeX + 1, placeY + 1].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX + 1, placeY + 1].getIndex());
                    }
                    else if ((array[placeX + 1, placeY + 1].getState()) && array[placeX + 1, placeY + 1].getInclusions())
                    {
                        tmpArray[placeX + 1, placeY + 1].setState(true);
                        tmpArray[placeX + 1, placeY + 1].setInclusions(true);
                    }
                    // grow middle bottom cell
                    if ((array[placeX, placeY + 1].getState()) && (array[placeX, placeY + 1].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX, placeY + 1].getIndex());
                    }
                    else if ((array[placeX, placeY + 1].getState()) && array[placeX, placeY + 1].getInclusions())
                    {
                        tmpArray[placeX, placeY + 1].setState(true);
                        tmpArray[placeX, placeY + 1].setInclusions(true);
                    }
                }
                // if nucleon is on left bottom corner
                else if (placeY == y - 1)
                {
                    // swap left bottom grow cell to right top cell of array
                    if (array[x - 1, 0].getState() && (array[x - 1, 0].getInclusions() == false))
                    {
                        neighborhood.Add(array[x - 1, 0].getIndex());
                    }
                    else if (array[x - 1, 0].getState() && array[x - 1, 0].getInclusions())
                    {
                        tmpArray[x - 1, 0].setState(true);
                        tmpArray[x - 1, 0].setInclusions(true);
                    }
                    // swap left middle grow cell to right last cell of array
                    if (array[x - 1, placeY].getState() && (array[x - 1, placeY].getInclusions() == false))
                    {
                        neighborhood.Add(array[x - 1, placeY].getIndex());

                    }
                    else if (array[x - 1, placeY].getState() && array[x - 1, placeY].getInclusions())
                    {
                        tmpArray[x - 1, placeY].setState(true);
                        tmpArray[x - 1, placeY].setInclusions(true);
                    }
                    // swap left top grow cell to right top cell of array
                    if (array[x - 1, placeY - 1].getState() && (array[x - 1, placeY - 1].getInclusions() == false))
                    {
                        neighborhood.Add(array[x - 1, placeY - 1].getIndex());
                    }
                    else if (array[x - 1, placeY - 1].getState() && array[x - 1, placeY - 1].getInclusions())
                    {
                        tmpArray[x - 1, placeY - 1].setState(true);
                        tmpArray[x - 1, placeY - 1].setInclusions(true);
                    }
                    // swap bottom middle grow cell to top cell of array
                    if (array[placeX, 0].getState() && (array[placeX, 0].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX, 0].getIndex());
                    }
                    else if (array[placeX, 0].getState() && array[placeX, 0].getInclusions())
                    {
                        tmpArray[placeX, 0].setState(true);
                        tmpArray[placeX, 0].setInclusions(true);
                    }
                    // swap bottom right grow cell to bottom cell of array
                    if (array[placeX + 1, 0].getState() && (array[placeX + 1, 0].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX + 1, 0].getIndex());
                    }
                    else if (array[placeX + 1, 0].getState() && array[placeX + 1, 0].getInclusions())
                    {
                        tmpArray[placeX + 1, 0].setState(true);
                        tmpArray[placeX + 1, 0].setInclusions(true);
                    }
                    // grow right middle cell
                    if (array[placeX + 1, placeY].getState() && (array[placeX + 1, placeY].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX + 1, placeY].getIndex());
                    }
                    else if (array[placeX + 1, placeY].getState() && array[placeX + 1, placeY].getInclusions())
                    {
                        tmpArray[placeX + 1, placeY].setState(true);
                        tmpArray[placeX + 1, placeY].setInclusions(true);
                    }
                    // grow right top cell
                    if (array[placeX + 1, placeY - 1].getState() && (array[placeX + 1, placeY - 1].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX + 1, placeY - 1].getIndex());
                    }
                    else if (array[placeX + 1, placeY - 1].getState() && array[placeX + 1, placeY - 1].getInclusions())
                    {
                        tmpArray[placeX + 1, placeY - 1].setState(true);
                        tmpArray[placeX + 1, placeY - 1].setInclusions(true);
                    }
                    // grow middle top cell
                    if (array[placeX, placeY - 1].getState() && (array[placeX, placeY - 1].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX, placeY - 1].getIndex());
                    }
                    else if (array[placeX, placeY - 1].getState() && array[placeX, placeY - 1].getInclusions())
                    {
                        tmpArray[placeX, placeY - 1].setState(true);
                        tmpArray[placeX, placeY - 1].setInclusions(true);
                    }
                }
                else
                {
                    // swap left top grow cell to right cell of array
                    if (array[x - 1, placeY - 1].getState() && (array[x - 1, placeY - 1].getInclusions() == false))
                    {
                        neighborhood.Add(array[x - 1, placeY - 1].getIndex());
                    }
                    else if (array[x - 1, placeY - 1].getState() && array[x - 1, placeY - 1].getInclusions())
                    {
                        tmpArray[x - 1, placeY - 1].setState(true);
                        tmpArray[x - 1, placeY - 1].setInclusions(true);
                    }
                    // swap left middle grow cell to right last cell of array
                    if (array[x - 1, placeY].getState() && (array[x - 1, placeY].getInclusions() == false))
                    {
                        neighborhood.Add(array[x - 1, placeY].getIndex());
                    }
                    else if (array[x - 1, placeY].getState() && array[x - 1, placeY].getInclusions())
                    {
                        tmpArray[x - 1, placeY].setState(true);
                        tmpArray[x - 1, placeY].setInclusions(true);
                    }
                    // swap left top grow cell to right top cell of array
                    if (array[x - 1, placeY + 1].getState() && (array[x - 1, placeY + 1].getInclusions() == false))
                    {
                        neighborhood.Add(array[x - 1, placeY + 1].getIndex());
                    }
                    else if (array[x - 1, placeY + 1].getState() && array[x - 1, placeY + 1].getInclusions())
                    {
                        tmpArray[x - 1, placeY + 1].setState(true);
                        tmpArray[x - 1, placeY + 1].setInclusions(true);
                    }
                    // grow top middle
                    if (array[placeX, placeY - 1].getState() && (array[placeX, placeY - 1].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX, placeY - 1].getIndex());
                    }
                    else if (array[placeX, placeY - 1].getState() && array[placeX, placeY - 1].getInclusions())
                    {
                        tmpArray[placeX, placeY - 1].setState(true);
                        tmpArray[placeX, placeY - 1].setInclusions(true);
                    }
                    // grow top right
                    if (array[placeX + 1, placeY - 1].getState() && (array[placeX + 1, placeY - 1].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX + 1, placeY - 1].getIndex());
                    }
                    else if (array[placeX + 1, placeY - 1].getState() && array[placeX + 1, placeY - 1].getInclusions())
                    {
                        tmpArray[placeX + 1, placeY - 1].setState(true);
                        tmpArray[placeX + 1, placeY - 1].setInclusions(true);
                    }
                    // grow middle right
                    if (array[placeX + 1, placeY].getState() && (array[placeX + 1, placeY].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX + 1, placeY].getIndex());
                    }
                    else if (array[placeX + 1, placeY].getState() && array[placeX + 1, placeY].getInclusions())
                    {
                        tmpArray[placeX + 1, placeY].setState(true);
                        tmpArray[placeX + 1, placeY].setInclusions(true);
                    }
                    //grow bottom right
                    if (array[placeX + 1, placeY + 1].getState() && (array[placeX + 1, placeY + 1].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX + 1, placeY + 1].getIndex());
                    }
                    else if (array[placeX + 1, placeY + 1].getState() && array[placeX + 1, placeY + 1].getInclusions())
                    {
                        tmpArray[placeX + 1, placeY + 1].setState(true);
                        tmpArray[placeX + 1, placeY + 1].setInclusions(true);
                    }
                    //grow middle bottom
                    if (array[placeX, placeY + 1].getState() && (array[placeX, placeY + 1].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX, placeY + 1].getIndex());
                    }
                    else if (array[placeX, placeY + 1].getState() && array[placeX, placeY + 1].getInclusions())
                    {
                        tmpArray[placeX, placeY + 1].setState(true);
                        tmpArray[placeX, placeY + 1].setInclusions(true);
                    }
                }
            }
            else if (placeX == x - 1)
            {
                // if nucleon is in top left corner
                if (placeY == 0)
                {
                    // swap right top grow cell to left bottom cell of array
                    if (array[0, y - 1].getState() && (array[0, y - 1].getInclusions() == false))
                    {
                        neighborhood.Add(array[0, y - 1].getIndex());
                    }
                    else if (array[0, y - 1].getState() && array[0, y - 1].getInclusions())
                    {
                        tmpArray[0, y - 1].setState(true);
                        tmpArray[0, y - 1].setInclusions(true);
                    }
                    // swap right middle grow cell to left first cell of array
                    if (array[0, placeY].getState() && (array[0, placeY].getInclusions() == false))
                    {
                        neighborhood.Add(array[0, placeY].getIndex());
                    }
                    else if (array[0, placeY].getState() && array[0, placeY].getInclusions())
                    {
                        tmpArray[0, placeY].setState(true);
                        tmpArray[0, placeY].setInclusions(true);
                    }
                    // swap right bottom grow cell to left first cell of array
                    if (array[0, placeY + 1].getState() && (array[0, placeY + 1].getInclusions() == false))
                    {
                        neighborhood.Add(array[0, placeY + 1].getIndex());
                    }
                    else if (array[0, placeY + 1].getState() && array[0, placeY + 1].getInclusions())
                    {
                        tmpArray[0, placeY + 1].setState(true);
                        tmpArray[0, placeY + 1].setInclusions(true);
                    }
                    // swap top middle grow cell to bottom cell of array
                    if (array[placeX, y - 1].getState() && (array[placeX, y - 1].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX, y - 1].getIndex());
                    }
                    else if (array[placeX, y - 1].getState() && array[placeX, y - 1].getInclusions())
                    {
                        tmpArray[placeX, y - 1].setState(true);
                        tmpArray[placeX, y - 1].setInclusions(true);
                    }
                    // swap top left grow cell to bottom cell of array
                    if (array[placeX - 1, y - 1].getState() && (array[placeX - 1, y - 1].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX - 1, y - 1].getIndex());
                    }
                    else if (array[placeX - 1, y - 1].getState() && array[placeX - 1, y - 1].getInclusions())
                    {
                        tmpArray[placeX - 1, y - 1].setState(true);
                        tmpArray[placeX - 1, y - 1].setInclusions(true);
                    }
                    // grow left middle cell
                    if (array[placeX - 1, placeY].getState() && (array[placeX - 1, placeY].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX - 1, placeY].getIndex());
                    }
                    else if (array[placeX - 1, placeY].getState() && array[placeX - 1, placeY].getInclusions())
                    {
                        tmpArray[placeX - 1, placeY].setState(true);
                        tmpArray[placeX - 1, placeY].setInclusions(true);
                    }
                    // grow left bottom cell
                    if (array[placeX - 1, placeY + 1].getState() && (array[placeX - 1, placeY + 1].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX - 1, placeY + 1].getIndex());
                    }
                    else if (array[placeX - 1, placeY + 1].getState() && array[placeX - 1, placeY + 1].getInclusions())
                    {
                        tmpArray[placeX - 1, placeY + 1].setState(true);
                        tmpArray[placeX - 1, placeY + 1].setInclusions(true);
                    }
                    // grow middle bottom cell
                    if (array[placeX, placeY + 1].getState() && (array[placeX, placeY + 1].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX, placeY + 1].getIndex());
                    }
                    else if (array[placeX, placeY + 1].getState() && array[placeX, placeY + 1].getInclusions())
                    {
                        tmpArray[placeX, placeY + 1].setState(true);
                        tmpArray[placeX, placeY + 1].setInclusions(true);
                    }
                }
                // if nucleon is on left bottom corner
                else if (placeY == y - 1)
                {
                    // swap right bottom grow cell to left top cell of array
                    if (array[0, 0].getState() && (array[0, 0].getInclusions() == false))
                    {
                        neighborhood.Add(array[0, 0].getIndex());
                    }
                    else if (array[0, 0].getState() && array[0, 0].getInclusions())
                    {
                        tmpArray[0, 0].setState(true);
                        tmpArray[0, 0].setInclusions(true);
                    }
                    // swap right middle grow cell to left first cell of array
                    if (array[0, placeY].getState() && (array[0, placeY].getInclusions() == false))
                    {
                        neighborhood.Add(array[0, placeY].getIndex());
                    }
                    else if (array[0, placeY].getState() && array[0, placeY].getInclusions())
                    {
                        tmpArray[0, placeY].setState(true);
                        tmpArray[0, placeY].setInclusions(true);
                    }
                    // swap right top grow cell to left cell of array
                    if (array[0, placeY - 1].getState() && (array[0, placeY - 1].getInclusions() == false))
                    {
                        neighborhood.Add(array[0, placeY - 1].getIndex());
                    }
                    else if (array[0, placeY - 1].getState() && array[0, placeY - 1].getInclusions())
                    {
                        tmpArray[0, placeY - 1].setState(true);
                        tmpArray[0, placeY - 1].setInclusions(true);
                    }
                    // swap bottom middle grow cell to top cell of array
                    if (array[placeX, 0].getState() && (array[placeX, 0].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX, 0].getIndex());
                    }
                    else if (array[placeX, 0].getState() && array[placeX, 0].getInclusions())
                    {
                        tmpArray[placeX, 0].setState(true);
                        tmpArray[placeX, 0].setInclusions(true);
                    }
                    // swap bottom left grow cell to top cell of array
                    if (array[placeX - 1, 0].getState() && (array[placeX - 1, 0].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX - 1, 0].getIndex());
                    }
                    else if (array[placeX - 1, 0].getState() && array[placeX - 1, 0].getInclusions())
                    {
                        tmpArray[placeX - 1, 0].setState(true);
                        tmpArray[placeX - 1, 0].setInclusions(true);
                    }
                    // grow left middle cell
                    if (array[placeX - 1, placeY].getState() && (array[placeX - 1, placeY].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX - 1, placeY].getIndex());
                    }
                    else if (array[placeX - 1, placeY].getState() && array[placeX - 1, placeY].getInclusions())
                    {
                        tmpArray[placeX - 1, placeY].setState(true);
                        tmpArray[placeX - 1, placeY].setInclusions(true);
                    }
                    // grow left top cell
                    if (array[placeX - 1, placeY - 1].getState() && (array[placeX - 1, placeY - 1].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX - 1, placeY - 1].getIndex());
                    }
                    else if (array[placeX - 1, placeY - 1].getState() && array[placeX - 1, placeY - 1].getInclusions())
                    {
                        tmpArray[placeX - 1, placeY - 1].setState(true);
                        tmpArray[placeX - 1, placeY - 1].setInclusions(true);
                    }
                    // grow middle top cell
                    if (array[placeX, placeY - 1].getState() && (array[placeX, placeY - 1].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX, placeY - 1].getIndex());
                    }
                    else if (array[placeX, placeY - 1].getState() && array[placeX, placeY - 1].getInclusions())
                    {
                        tmpArray[placeX, placeY - 1].setState(true);
                        tmpArray[placeX, placeY - 1].setInclusions(true);
                    }
                }
                else
                {
                    // swap right top grow cell to left cell of array
                    if (array[0, placeY - 1].getState() && (array[0, placeY - 1].getInclusions() == false))
                    {
                        neighborhood.Add(array[0, placeY - 1].getIndex());
                    }
                    else if (array[0, placeY - 1].getState() && array[0, placeY - 1].getInclusions())
                    {
                        tmpArray[0, placeY - 1].setState(true);
                        tmpArray[0, placeY - 1].setInclusions(true);
                    }
                    // swap right middle grow cell to left first cell of array
                    if (array[0, placeY].getState() && (array[0, placeY].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX, placeY - 1].getIndex());
                    }
                    else if (array[0, placeY].getState() && array[0, placeY].getInclusions())
                    {
                        tmpArray[0, placeY].setState(true);
                        tmpArray[0, placeY].setInclusions(true);
                    }
                    // swap right bottom grow cell to left cell of array
                    if (array[0, placeY + 1].getState() && (array[0, placeY + 1].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX, placeY - 1].getIndex());
                    }
                    else if (array[0, placeY + 1].getState() && array[0, placeY + 1].getInclusions())
                    {
                        tmpArray[0, placeY + 1].setState(true);
                        tmpArray[0, placeY + 1].setInclusions(true);
                    }
                    // grow top middle
                    if (array[placeX, placeY - 1].getState() && (array[placeX, placeY - 1].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX, placeY - 1].getIndex());
                    }
                    else if (array[placeX, placeY - 1].getState() && array[placeX, placeY - 1].getInclusions())
                    {
                        tmpArray[placeX, placeY - 1].setState(true);
                        tmpArray[placeX, placeY - 1].setInclusions(true);
                    }
                    // grow top left
                    if (array[placeX - 1, placeY - 1].getState() && (array[placeX - 1, placeY - 1].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX, placeY - 1].getIndex());
                    }
                    else if (array[placeX - 1, placeY - 1].getState() && array[placeX - 1, placeY - 1].getInclusions())
                    {
                        tmpArray[placeX - 1, placeY - 1].setState(true);
                        tmpArray[placeX - 1, placeY - 1].setInclusions(true);
                    }
                    // grow middle left
                    if (array[placeX - 1, placeY].getState() && (array[placeX - 1, placeY].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX, placeY - 1].getIndex());
                    }
                    else if (array[placeX - 1, placeY].getState() && array[placeX - 1, placeY].getInclusions())
                    {
                        tmpArray[placeX - 1, placeY].setState(true);
                        tmpArray[placeX - 1, placeY].setInclusions(true);
                    }
                    //grow bottom left
                    if (array[placeX - 1, placeY + 1].getState() && (array[placeX - 1, placeY + 1].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX, placeY - 1].getIndex());
                    }
                    else if (array[placeX - 1, placeY + 1].getState() && array[placeX - 1, placeY + 1].getInclusions())
                    {
                        tmpArray[placeX - 1, placeY + 1].setState(true);
                        tmpArray[placeX - 1, placeY + 1].setInclusions(true);
                    }
                    //grow middle bottom
                    if (array[placeX, placeY + 1].getState() && (array[placeX, placeY + 1].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX, placeY - 1].getIndex());
                    }
                    else if (array[placeX, placeY + 1].getState() && array[placeX, placeY + 1].getInclusions())
                    {
                        tmpArray[placeX, placeY + 1].setState(true);
                        tmpArray[placeX, placeY + 1].setInclusions(true);
                    }
                }
            }
            else
            {
                if (placeY == 0)
                {
                    // swap right top grow cell to left bottom cell of array
                    if (array[placeX - 1, y - 1].getState() && (array[placeX - 1, y - 1].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX - 1, y - 1].getIndex());
                    }
                    else if (array[placeX - 1, y - 1].getState() && array[placeX - 1, y - 1].getInclusions())
                    {
                        tmpArray[placeX - 1, y - 1].setState(true);
                        tmpArray[placeX - 1, y - 1].setInclusions(true);
                    }
                    // swap top middle grow cell to bottom cell of array
                    if (array[placeX, y - 1].getState() && (array[placeX, y - 1].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX, y - 1].getIndex());
                    }
                    else if (array[placeX, y - 1].getState() && array[placeX, y - 1].getInclusions())
                    {
                        tmpArray[placeX, y - 1].setState(true);
                        tmpArray[placeX, y - 1].setInclusions(true);
                    }
                    // swap right top grow cell to bottom cell of array
                    if (array[placeX + 1, y - 1].getState() && (array[placeX + 1, y - 1].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX + 1, y - 1].getIndex());
                    }
                    else if (array[placeX + 1, y - 1].getState() && array[placeX + 1, y - 1].getInclusions())
                    {
                        tmpArray[placeX + 1, y - 1].setState(true);
                        tmpArray[placeX + 1, y - 1].setInclusions(true);
                    }
                    // grow right middle cell
                    if (array[placeX + 1, placeY].getState() && (array[placeX + 1, placeY].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX + 1, placeY].getIndex());
                    }
                    else if (array[placeX + 1, placeY].getState() && array[placeX + 1, placeY].getInclusions())
                    {
                        tmpArray[placeX + 1, placeY].setState(true);
                        tmpArray[placeX + 1, placeY].setInclusions(true);
                    }
                    // grow right bottom cell
                    if (array[placeX + 1, placeY + 1].getState() && (array[placeX + 1, placeY + 1].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX + 1, placeY + 1].getIndex());
                    }
                    else if (array[placeX + 1, placeY + 1].getState() && array[placeX + 1, placeY + 1].getInclusions())
                    {
                        tmpArray[placeX + 1, placeY + 1].setState(true);
                        tmpArray[placeX + 1, placeY + 1].setInclusions(true);
                    }
                    // grow bottom middle cell
                    if (array[placeX, placeY + 1].getState() && (array[placeX, placeY + 1].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX, placeY + 1].getIndex());
                    }
                    else if (array[placeX, placeY + 1].getState() && array[placeX, placeY + 1].getInclusions())
                    {
                        tmpArray[placeX, placeY + 1].setState(true);
                        tmpArray[placeX, placeY + 1].setInclusions(true);
                    }
                    // grow left bottom cell
                    if (array[placeX - 1, placeY + 1].getState() && (array[placeX - 1, placeY + 1].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX - 1, placeY + 1].getIndex());
                    }
                    else if (array[placeX - 1, placeY + 1].getState() && array[placeX - 1, placeY + 1].getInclusions())
                    {
                        tmpArray[placeX - 1, placeY + 1].setState(true);
                        tmpArray[placeX - 1, placeY + 1].setInclusions(true);
                    }
                    // grow middle left cell
                    if (array[placeX - 1, placeY].getState() && (array[placeX - 1, placeY].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX - 1, placeY].getIndex());
                    }
                    else if (array[placeX - 1, placeY].getState() && array[placeX - 1, placeY].getInclusions())
                    {
                        tmpArray[placeX - 1, placeY].setState(true);
                        tmpArray[placeX - 1, placeY].setInclusions(true);
                    }
                }
                // if nucleon is on left bottom corner
                else if (placeY == y - 1)
                {
                    // swap right bottom grow cell to top cell of array
                    if (array[placeX + 1, 0].getState() && (array[placeX + 1, 0].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX + 1, 0].getIndex());
                    }
                    else if (array[placeX + 1, 0].getState() && array[placeX + 1, 0].getInclusions())
                    {
                        tmpArray[placeX + 1, 0].setState(true);
                        tmpArray[placeX + 1, 0].setInclusions(true);
                    }
                    // grow right middle
                    if (array[placeX + 1, placeY].getState() && (array[placeX + 1, placeY].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX + 1, placeY].getIndex());
                    }
                    else if (array[placeX + 1, placeY].getState() && array[placeX + 1, placeY].getInclusions())
                    {
                        tmpArray[placeX + 1, placeY].setState(true);
                        tmpArray[placeX + 1, placeY].setInclusions(true);
                    }
                    // grow right top
                    if (array[placeX + 1, placeY - 1].getState() && (array[placeX + 1, placeY - 1].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX + 1, placeY - 1].getIndex());
                    }
                    else if (array[placeX + 1, placeY - 1].getState() && array[placeX + 1, placeY - 1].getInclusions())
                    {
                        tmpArray[placeX + 1, placeY - 1].setState(true);
                        tmpArray[placeX + 1, placeY - 1].setInclusions(true);
                    }
                    // grow top middle
                    if (array[placeX, placeY - 1].getState() && (array[placeX, placeY - 1].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX, placeY - 1].getIndex());
                    }
                    else if (array[placeX, placeY - 1].getState() && array[placeX, placeY - 1].getInclusions())
                    {
                        tmpArray[placeX, placeY - 1].setState(true);
                        tmpArray[placeX, placeY - 1].setInclusions(true);
                    }
                    // grow left top
                    if (array[placeX - 1, placeY - 1].getState() && (array[placeX - 1, placeY - 1].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX - 1, placeY - 1].getIndex());
                    }
                    else if (array[placeX - 1, placeY - 1].getState() && array[placeX - 1, placeY - 1].getInclusions())
                    {
                        tmpArray[placeX - 1, placeY - 1].setState(true);
                        tmpArray[placeX - 1, placeY - 1].setInclusions(true);
                    }
                    // grow left middle cell
                    if (array[placeX - 1, placeY].getState() && (array[placeX - 1, placeY].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX - 1, placeY].getIndex());
                    }
                    else if (array[placeX - 1, placeY].getState() && array[placeX - 1, placeY].getInclusions())
                    {
                        tmpArray[placeX - 1, placeY].setState(true);
                        tmpArray[placeX - 1, placeY].setInclusions(true);
                    }
                    // swap left bottom cell to top cell of array
                    if (array[placeX - 1, 0].getState() && (array[placeX - 1, 0].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX - 1, 0].getIndex());
                    }
                    else if (array[placeX - 1, 0].getState() && array[placeX - 1, 0].getInclusions())
                    {
                        tmpArray[placeX - 1, 0].setState(true);
                        tmpArray[placeX - 1, 0].setInclusions(true);
                    }
                    // grow middle bottom cell - swap to top
                    if (array[placeX, 0].getState() && (array[placeX, 0].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX, 0].getIndex());
                    }
                    else if (array[placeX, 0].getState() && array[placeX, 0].getInclusions())
                    {
                        tmpArray[placeX, 0].setState(true);
                        tmpArray[placeX, 0].setInclusions(true);
                    }
                }
                else
                {
                    // grow top left
                    if (array[placeX - 1, placeY - 1].getState() && (array[placeX - 1, placeY - 1].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX - 1, placeY - 1].getIndex());
                    }
                    else if (array[placeX - 1, placeY - 1].getState() && array[placeX - 1, placeY - 1].getInclusions())
                    {
                        tmpArray[placeX - 1, placeY - 1].setState(true);
                        tmpArray[placeX - 1, placeY - 1].setInclusions(true);
                    }
                    // grow left middle
                    if (array[placeX - 1, placeY].getState() && (array[placeX - 1, placeY].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX - 1, placeY].getIndex());
                    }
                    else if (array[placeX - 1, placeY].getState() && array[placeX - 1, placeY].getInclusions())
                    {
                        tmpArray[placeX - 1, placeY].setState(true);
                        tmpArray[placeX - 1, placeY].setInclusions(true);
                    }
                    // grow left bottom
                    if (array[placeX - 1, placeY + 1].getState() && (array[placeX - 1, placeY + 1].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX - 1, placeY + 1].getIndex());
                    }
                    else if (array[placeX - 1, placeY + 1].getState() && array[placeX - 1, placeY + 1].getInclusions())
                    {
                        tmpArray[placeX - 1, placeY + 1].setState(true);
                        tmpArray[placeX - 1, placeY + 1].setInclusions(true);
                    }
                    // grow top middle
                    if (array[placeX, placeY - 1].getState() && (array[placeX, placeY - 1].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX, placeY - 1].getIndex());
                    }
                    else if (array[placeX, placeY - 1].getState() && array[placeX, placeY - 1].getInclusions())
                    {
                        tmpArray[placeX, placeY - 1].setState(true);
                        tmpArray[placeX, placeY - 1].setInclusions(true);
                    }
                    // grow top left
                    if (array[placeX + 1, placeY - 1].getState() && (array[placeX + 1, placeY - 1].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX + 1, placeY - 1].getIndex());
                    }
                    else if (array[placeX + 1, placeY - 1].getState() && array[placeX + 1, placeY - 1].getInclusions())
                    {
                        tmpArray[placeX + 1, placeY - 1].setState(true);
                        tmpArray[placeX + 1, placeY - 1].setInclusions(true);
                    }
                    // grow middle right
                    if (array[placeX + 1, placeY].getState() && (array[placeX + 1, placeY].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX + 1, placeY].getIndex());
                    }
                    else if (array[placeX + 1, placeY].getState() && array[placeX + 1, placeY].getInclusions())
                    {
                        tmpArray[placeX + 1, placeY].setState(true);
                        tmpArray[placeX + 1, placeY].setInclusions(true);
                    }
                    //grow bottom right
                    if (array[placeX + 1, placeY + 1].getState() && (array[placeX + 1, placeY + 1].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX + 1, placeY + 1].getIndex());
                    }
                    else if (array[placeX + 1, placeY + 1].getState() && array[placeX + 1, placeY + 1].getInclusions())
                    {
                        tmpArray[placeX + 1, placeY + 1].setState(true);
                        tmpArray[placeX + 1, placeY + 1].setInclusions(true);
                    }
                    //grow middle bottom
                    if (array[placeX, placeY + 1].getState() && (array[placeX, placeY + 1].getInclusions() == false))
                    {
                        neighborhood.Add(array[placeX, placeY + 1].getIndex());
                    }
                    else if (array[placeX, placeY + 1].getState() && array[placeX, placeY + 1].getInclusions())
                    {
                        tmpArray[placeX, placeY + 1].setState(true);
                        tmpArray[placeX, placeY + 1].setInclusions(true);
                    }
                }
            }

            if (neighborhood.Any())
            {
                tmpArray[placeX, placeY].setState(true);
                tmpArray[placeX, placeY].setIndex(neighborhood.GroupBy(v => v).First(g => g.Count() == neighborhood.GroupBy(v => v).Max(v => v.Count())).Key);
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            
        }
        //simulation
        private void button1_Click(object sender, EventArgs e)
        {
            if (growNucleonsFinished)
            {
                typeOfSimulation = simulationType.Text;

                do
                {
                    counter = 0;
                    inspectArray();
                    rewriteArray();
                    drawPictureBox();

                    //delay
                    for (int i = 0; i < 1; i++)
                    {
                        Application.DoEvents();
                    }
                } while (counter > 0);
                growNucleonsFinished = true;
            }
            else
            {
                MessageBox.Show("Generate new Bitmap to grow new necleons.");
            }
        }

        //export to txt file
        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            using (StreamWriter export = new StreamWriter("microstructure.txt"))
            {
                export.WriteLine(x + " " + y + " " + cellSize);
                for (int i = 0; i < x; i++)
                    for (int j = 0; j < y; j++)
                        export.WriteLine(i + " " + j + " " + array[i, j].getState() + " " + array[i, j].getIndex());
            }
        }

        //export to BMP file
        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            pictureBox1.Image.Save("microstructure.bmp", System.Drawing.Imaging.ImageFormat.Bmp);
        }

        //import from txt file
        private void importTXT_Click(object sender, EventArgs e)
        {

            using (StreamReader import = new StreamReader(@"microstructure.txt"))
            {
                bool firstLine = true;
                string[] values;
                int i = 0;  // counter for do while loop
                do
                {
                    values = import.ReadLine().Split();

                    if (firstLine)
                    {
                        int nAmount;
                        x = int.Parse(values[0]);
                        y = int.Parse(values[1]);
                        pictureBox1.Size = new Size(x * cellSize, y * cellSize);
                        colorArray = new Color[x * y];
                        for (int j = 0; j < x * y; j++)
                        {
                            if (j == clickedNucleonIndex)
                            {
                                colorArray[j] = clickedNucleonColor;
                            }
                            else
                            {
                                colorArray[j] = Color.FromArgb(rand.Next(256), rand.Next(256), rand.Next(256));
                            }
                        }
                        array = new Cell[x, y];
                        //create initial array
                        array = setInitialArray(array);
                        nAmount = int.Parse(values[2]);
                        firstLine = false;
                    }
                    else
                    {
                        int posX, posY, index;
                        posX = int.Parse(values[0]);
                        posY = int.Parse(values[1]);
                        index = int.Parse(values[3]);
                        array[posX, posY].setState(true);
                        array[posX, posY].setIndex(index);
                    }
                    //++number of reading line and go to next
                    i++;
                } while (i <= (x * y));
            }
            drawPictureBox();
        }

        //import from BMP file
        private void importBMP_Click(object sender, EventArgs e)
        {
            colorArray = new Color[x * y];
            for (int i = 0; i < x * y; i++)
            {
                if (myHashMap1.ContainsKey(i))
                {
                    colorArray[i] = myHashMap1[i];
                }
                else
                {
                    colorArray[i] = Color.FromArgb(rand.Next(256), rand.Next(256), rand.Next(256));
                }
            }
            Bitmap microstructure;
            microstructure = new Bitmap(@"microstructure.bmp", true);
            colorHashMap.Clear(); //clear hashMap with colors-becouse generate new colors after import bitmap
            // Set the PictureBox to display the image
            pictureBox1.Image = microstructure;
            int loopLimitHeight = microstructure.Height;
            int loopLimitWidth = microstructure.Width;
            for (int i = 0; i < loopLimitWidth; i++)
            {
                for (int j = 0; j < loopLimitHeight; j++)
                {
                    Color pixelColor = microstructure.GetPixel(i, j);  // set color from BMP pixel to pixelColor (R,G,B)
                    if (colorHashMap.ContainsKey(pixelColor) == false)  // check if pixelColor is already in hashMap
                    {
                        int randIndex = rand.Next(loopLimitWidth * loopLimitHeight);
                        colorHashMap[pixelColor] = randIndex;
                    }
                    if (colorHashMap.ContainsKey(pixelColor))
                    {
                        array[i, j].setIndex(colorHashMap[pixelColor]);
                        array[i, j].setState(true);
                        array[i, j].setInclusions(false);
                        array[i, j].setBoundary(false);
                    }
                }
                
            }
            generateNucleonsFinished = true;
            growNucleonsFinished = true;
            drawPictureBox();
        }

        //generate inclusions
        private void button2_Click(object sender, EventArgs e)
        {
            inclusions = Int32.Parse(inclusion.Text);
            fillArrayWithRandomInclusions();
        }

        private void textBox1_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void generateNucleons_Click(object sender, EventArgs e)
        {
            if (generateNucleonsFinished == false)
            {
                nucleons = Int32.Parse(nucleonAmount.Text);

                colorArray = new Color[x * y];
                for (int i = 0; i < x * y; i++)
                {
                    if (myHashMap1.ContainsKey(i))
                    {
                        colorArray[i] = myHashMap1[i];
                    }
                    else
                    {
                        colorArray[i] = Color.FromArgb(rand.Next(256), rand.Next(256), rand.Next(256));
                    }
                }

                fillArrayWithRandomNucleons();
                growNucleonsFinished = true;
            }
            else
            {
                MessageBox.Show("You already generate nucleons. Please simulate them grown or generate new Bitmap.");
            }
        }

        private void nextStepButton_Click(object sender, EventArgs e)
        {
            inspectArray();
            rewriteArray();
            drawPictureBox();
        }
        //generate bitmap
        private void button2_Click_1(object sender, EventArgs e)
        {
            x = Int32.Parse(sizeX.Text);
            y = Int32.Parse(sizeY.Text);
            if (x > 0 && x <= 600 && y > 0 && y <= 600)
            {
                array = new Cell[x, y];
                tmpArray = new Cell[x, y];

                //create initial arrays
                array = setInitialArray(array);
                tmpArray = setInitialArray(tmpArray);

                createBmp = new Bitmap(x * cellSize, y * cellSize);
                createGraphic = Graphics.FromImage(createBmp);
                for (int i = 0; i < x; i++)
                {
                    for (int j = 0; j < y; j++)
                    {
                        createGraphic.FillRectangle(Brushes.White, i * cellSize, j * cellSize, cellSize, cellSize);
                    }
                }
                pictureBox1.Image = createBmp;
                generateInclusionFinished = false;
                growNucleonsFinished = false;
                generateNucleonsFinished = false;
            }
            else
            {
                MessageBox.Show("Array size is not valid. Try one more time.");
                return;
            }

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void symulationType_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        public void shapeControll(int i, int j)
        {
            int left, right, bottom, top;
            List<int> neighborhood = new List<int>(); // list for neighbors index
            neighborhood.Clear();
            // check if     
            if (i == 0) left = x - 1; else left = i - 1;
            if (i == x - 1) right = 0; else right = i + 1;
            if (j == 0) top = y - 1; else top = j - 1;
            if (j == y - 1) bottom = 0; else bottom = j + 1;

            if (array[left, top].getState())
            {
                neighborhood.Add(array[left, top].getIndex());
            }
            if (array[i, top].getState())
            {
                neighborhood.Add(array[i, top].getIndex()); 
            }
            if (array[right, top].getState())
            {
                neighborhood.Add(array[right, top].getIndex());
            }
            if (array[left, j].getState())
            {
                neighborhood.Add(array[left, j].getIndex());
            }
            if (array[right, j].getState())
            {
                neighborhood.Add(array[right, j].getIndex());
            }
            if (array[left, bottom].getState())
            {
                neighborhood.Add(array[left, bottom].getIndex());
            }
            if (array[i, bottom].getState())
            {
                neighborhood.Add(array[i, bottom].getIndex());
            }
            if (array[right, bottom].getState())
            {
                neighborhood.Add(array[right, bottom].getIndex());
            }
            if (neighborhood.Any())
            {
            //rule 1
                if (neighborhood.GroupBy(index => index).Max(index => index.Count()) >= 5)
                {
                    tmpArray[i, j].setState(true);
                    tmpArray[i, j].setIndex(neighborhood.GroupBy(index => index)
                                                    .First(indexNum => indexNum.Count() == neighborhood.GroupBy(v => v).Max(index => index.Count()))
                                                    .Key);
                }
                else
                { //rule 2
                    neighborhood.Clear();
                    if (array[i, top].getState())
                    {
                        neighborhood.Add(array[i, top].getIndex());
                    }
                    if (array[left, j].getState())
                    {
                        neighborhood.Add(array[left, j].getIndex());
                    }
                    if (array[right, j].getState())
                    {
                        neighborhood.Add(array[right, j].getIndex());
                    }
                    if (array[i, bottom].getState())
                    {
                        neighborhood.Add(array[i, bottom].getIndex());
                    }
                    if (neighborhood.Any() && neighborhood.GroupBy(v => v).Max(g => g.Count()) >= 3)
                    {
                        tmpArray[i, j].setState(true);
                        tmpArray[i, j].setIndex(neighborhood.GroupBy(index => index)
                                                            .First(indexNum => indexNum.Count() == neighborhood.GroupBy(v => v).Max(index => index.Count()))
                                                            .Key);
                    }
                    else
                    { //rule 3
                        neighborhood.Clear();
                        if (array[left, top].getState())
                        {
                            neighborhood.Add(array[left, top].getIndex());
                        }
                        if (array[right, top].getState())
                        {
                            neighborhood.Add(array[right, top].getIndex());
                        }
                        if (array[left, bottom].getState())
                        {
                            neighborhood.Add(array[left, bottom].getIndex());
                        }
                        if (array[right, bottom].getState())
                        {
                            neighborhood.Add(array[right, bottom].getIndex());
                        }
                        if (neighborhood.Any() && neighborhood.GroupBy(v => v).Max(g => g.Count()) >= 3)
                        {
                            tmpArray[i, j].setState(true);
                            tmpArray[i, j].setIndex(neighborhood.GroupBy(index => index)
                                                                .First(indexNum => indexNum.Count() == neighborhood.GroupBy(v => v).Max(index => index.Count()))
                                                                .Key);
                        }
                        else
                        {  //rule 4
                            neighborhood.Clear();
                            if (array[left, top].getState())
                            {
                                neighborhood.Add(array[left, top].getIndex());
                            }
                            if (array[i, top].getState())
                            {
                                neighborhood.Add(array[i, top].getIndex());
                            }
                            if (array[right, top].getState())
                            {
                                neighborhood.Add(array[right, top].getIndex());
                            }
                            if (array[left, j].getState())
                            {
                                neighborhood.Add(array[left, j].getIndex());
                            }
                            if (array[right, j].getState())
                            {
                                neighborhood.Add(array[right, j].getIndex());
                            }
                            if (array[left, bottom].getState())
                            {
                                neighborhood.Add(array[left, bottom].getIndex());
                            }
                            if (array[i, bottom].getState())
                            {
                                neighborhood.Add(array[i, bottom].getIndex());
                            }
                            if (array[right, bottom].getState())
                            {
                                neighborhood.Add(array[right, bottom].getIndex());
                            }
                            if (rand.Next(0, 100) < scrollBarProbability.Value)
                            {
                                tmpArray[i, j].setState(true);
                                tmpArray[i, j].setIndex(neighborhood.GroupBy(index => index)
                                                                    .First(indexNum => indexNum.Count() == neighborhood.GroupBy(v => v).Max(index => index.Count()))
                                                                    .Key);
                            }
                        }
                    }
                }
            }
        }

        private void scrollBarProbability_Scroll(object sender, ScrollEventArgs e)
        {

        }

        private void growWithShapeControl_Click(object sender, EventArgs e)
        {
            
        }
        // generate boundaries btn
        private void button2_Click_2(object sender, EventArgs e)
        {
            generateBoundaries();
            drawPictureBox();
        }

        //get index to substructure and dual phase
        private void pictureBoxMouseUp(object sender, MouseEventArgs e)
        {
            mouseX = e.X;
            mouseY = e.Y;
            if (mouseX <= x && mouseY <= y)
            {
                clickedNucleonIndex = array[mouseX, mouseY].getIndex();
                clickedNucleonColor = createBmp.GetPixel(mouseX, mouseY);
                if (myHashMap1.ContainsKey(clickedNucleonIndex) == false) //if I dont have that index-save it and set color
                {
                    myHashMap1.Add(clickedNucleonIndex, clickedNucleonColor);
                }
            }
            nucleonsSelected = true;
        }


        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            if (generateBoundariesFinished)
            {
                for (int i = 0; i < x; i++)
                {
                    for (int j = 0; j < y; j++)
                    {
                        if (!array[i, j].getBoudary())
                        {
                            array[i, j].setState(false);
                            array[i, j].setIndex(0);
                            tmpArray[i, j].setState(false);
                            tmpArray[i, j].setIndex(0);
                        }
                    }
                }
                drawPictureBox();
            }
        } 
    }
}
