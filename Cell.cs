﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Multiscale
{
    public class Cell
    {
        private int index;  
        private bool state;
        private bool isInclusion;
        private bool isBoundary;
        private bool ignoreCell;
        public Cell()
        {
            this.index = 0;
            this.state = false;
            this.isInclusion = false;
            this.isBoundary = false;
            this.ignoreCell = false;
        }
        
        public void setIndex(int i) {
            this.index = i;
        }
        public void setState(bool s)
        {
            this.state = s;
        }
        public void setInclusions(bool inc)
        {
            this.isInclusion = inc;
        }
        public int getIndex()
        {
            return this.index;
        }
        public bool getState()
        {
            return this.state;
        }
        public bool getInclusions()
        {
            return this.isInclusion;
        }
        public bool getBoudary()
        {
            return this.isBoundary;
        }
        public void setBoundary(bool b)
        {
            this.isBoundary = b;
        }
        public void setIgnore(bool c)
        {
            this.ignoreCell = c;
        }
        public bool getIgnore() {
            return this.ignoreCell;
        }
        
    }
    
}

